INSERT INTO parametricas.estado (est_codigo, est_nombre, usuario_registro) VALUES(0, 'INACTIVO', 1);
INSERT INTO parametricas.estado (est_codigo, est_nombre, usuario_registro) VALUES(1, 'ACTIVO', 1);

INSERT INTO parametricas.estado_civil (estciv_codigo, estciv_nombre, usuario_registro) VALUES(0, 'NO DEFINIDO', 1);
INSERT INTO parametricas.estado_civil (estciv_codigo, estciv_nombre, usuario_registro) VALUES(1, 'SOLTERO(A)', 1);
INSERT INTO parametricas.estado_civil (estciv_codigo, estciv_nombre, usuario_registro) VALUES(2, 'CASADO(A)', 1);
INSERT INTO parametricas.estado_civil (estciv_codigo, estciv_nombre, usuario_registro) VALUES(3, 'DIVORCIADO(A)', 1);
INSERT INTO parametricas.estado_civil (estciv_codigo, estciv_nombre, usuario_registro) VALUES(4, 'VIUDO(A)', 1);

INSERT INTO parametricas.especialidad (esp_codigo, esp_nombre, usuario_registro) VALUES (1, 'Medicamentos', 0);
INSERT INTO parametricas.especialidad (esp_codigo, esp_nombre, usuario_registro) VALUES (2, 'Cosmeticos', 0);
INSERT INTO parametricas.especialidad (esp_codigo, esp_nombre, usuario_registro) VALUES (3, 'Dispositivos', 0);
INSERT INTO parametricas.especialidad (esp_codigo, esp_nombre, usuario_registro) VALUES (4, 'Limpieza', 0);

INSERT INTO parametricas.genero (gen_codigo, gen_nombre, usuario_registro) VALUES(0, 'NO DEFINIDO', 1);
INSERT INTO parametricas.genero (gen_codigo, gen_nombre, usuario_registro) VALUES(1, 'MASCULINO', 1);
INSERT INTO parametricas.genero (gen_codigo, gen_nombre, usuario_registro) VALUES(2, 'FEMENINO', 1);

INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(0, 'NO DEFINIDO', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(1, 'CARDIOVASCULARES', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(2, 'DIGESTIVOS', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(3, 'DERMATOLOGICOS', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(4, 'GINECOLÓGICOS', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(5, 'ASMA', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(6, 'RESPIRATORIOS', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(7, 'NEFROURINARIOS', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(8, 'DIABETES(tipo)', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(9, 'EPITÉPTICOS', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_afeccion (tipafe_codigo, tipafe_nombre, tipafe_descripcion, usuario_registro) VALUES(10, 'HÁBITOS', 'DESCRIPCION', 1);

INSERT INTO parametricas.tipo_documento (tipdoc_codigo, tipdoc_nombre, usuario_registro) VALUES(0, 'NO DEFINIDO', 1);
INSERT INTO parametricas.tipo_documento (tipdoc_codigo, tipdoc_nombre, usuario_registro) VALUES(1, 'CÉDULA DE IDENTIDAD', 1);
INSERT INTO parametricas.tipo_documento (tipdoc_codigo, tipdoc_nombre, usuario_registro) VALUES(2, 'CÉDULA DE IDENTIDAD DE EXTRANJERIA', 1);

INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(0, 'NO DEFINIDO', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(1, 'REHABILITACIÓN ORAL Y ESTÉTICA', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(2, 'ODONTOPEDIATRÍA', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(3, 'IMPLANTOLOGÍA', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(4, 'ENDODONCIA', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(5, 'ORTODONCIA', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(6, 'PERIODONCIA', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(7, 'CIRUGÍA BUCAL', 'DESCRIPCION', 1);
INSERT INTO parametricas.tipo_tratamiento (tiptra_codigo, tiptra_nombre, tiptra_descripcion, usuario_registro) VALUES(8, '', 'DESCRIPCION', 1);

INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(1, null, 1, 'Persona', 'mdi-account', '/dental/personas', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(2, null, 2, 'Personal Odontológico', 'mdi-doctor', '/dental/personal-odontologico', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(3, null, 3, 'Paciente', 'mdi-account-alert', '/dental/paciente', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(4, null, 4, 'Antecedentes', 'mdi-folder-open', '/dental/antecedentes', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(5, null, 5, 'Tratamiento', 'mdi-medical-bag', '/dental/tratamiento', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(6, null, 6, 'Reservas', 'mdi-phone', '/dental/reservas', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(7, null, 7, 'Consulta', 'mdi-tooth', '/dental/consulta', 1);

INSERT INTO autenticacion.rol (rol_codigo, rol_nombre, rol_descripcion, usuario_registro) VALUES(1, 'Adminsitrador', '', 1);
INSERT INTO autenticacion.rol (rol_codigo, rol_nombre, rol_descripcion, usuario_registro) VALUES(2, 'Odontologo', '', 1);

INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(1, 1, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(1, 2, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(1, 3, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(1, 4, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(1, 5, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(1, 6, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(2, 4, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(2, 5, 1);
INSERT INTO autenticacion.rol_menu (rol_codigo, men_codigo, usuario_registro) VALUES(2, 7, 1);

INSERT INTO registro.persona (per_codigo,tipdoc_codigo,gen_codigo,per_docidentidad,per_nombres,per_primer_apellido,per_segundo_apellido,per_fecha_nacimiento,per_celular,per_correo,per_estado,usuario_registro,usuario_modificacion,usuario_baja,fecha_registro,fecha_modificacion,fecha_baja) VALUES (1,1,2,'92525269','Admin','Admin','Admin','1998-10-25','+591 2222222','admin@correo.com',1,100,1,0,'2022-08-24 18:40:24.92679','2022-08-26 16:32:07.598','1999-01-01 00:00:00');

INSERT INTO autenticacion.usuario (usu_codigo,per_codigo,usu_usuario,usu_contrasenia,usu_estado,usuario_registro,usuario_modificacion,usuario_baja,fecha_registro,fecha_modificacion,fecha_baja) VALUES (1,1,'admin','i7DPbrmxfQ99IrRW8SElfcElTh8BZlNwR2OD6ndt9BQ=',1,100,100,100,'2022-08-24 18:41:18.770425','2022-08-24 16:20:04.129','2022-08-24 14:41:28.965');

INSERT INTO autenticacion.usuario_rol (usu_codigo, rol_codigo, usurol_fechaexpiracion, usuario_registro) VALUES(1, 1, '12/12/2025', 1);
INSERT INTO autenticacion.usuario_rol (usu_codigo, rol_codigo, usurol_fechaexpiracion, usuario_registro) VALUES(1, 2, '12/12/2025', 1);
