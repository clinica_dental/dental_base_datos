# Dental Base de Datos
 
## Proyecto dental base de datos
 
### Instalación
 
> :warning: **Usar Postgres v11 o superior**:
 
Clonar el proyecto desde el repositorio
```bash
git clone https://gitlab.com/proyecto_dental/dental_compilados/dental_base_datos.git
cd dental_base_datos
```
 
La creacion de la base de datos, roles, esquemas, tablas y relaciones se encuentra en el archivo **db_dental.sql** se debe ejecutar todo su contenido en una base de datos nueva.
 
Los valores predefinidos de las tablas parametricas deben insertarse usando los archivos de la carpeta **seeders**

seeders
  * estado_civil.sql
  * estados.sql
  * genero.sql
  * menu.sql
  * persona.sql
  * rol_menu.sql
  * rol.sql
  * tipo_afeccion.sql
  * tipo_documento.sql
  * tipo_tratamiento.sql
  * usuario_rol.sql
  * usuario.sql

