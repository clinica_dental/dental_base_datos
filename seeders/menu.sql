INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(1, null, 1, 'Persona', 'mdi-account', '/personas', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(2, null, 2, 'Personal Odontológico', 'mdi-doctor', '/personal-odontologico', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(3, null, 3, 'Paciente', 'mdi-account-alert', '/paciente', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(4, null, 4, 'Antecedentes', 'mdi-folder-open', '/antecedentes', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(5, null, 5, 'Tratamiento', 'mdi-medical-bag', '/tratamiento', 1);
INSERT INTO autenticacion.menu (men_codigo, men_codigo_padre, men_orden, men_nombre, men_icono, men_ruta, usuario_registro) VALUES(6, null, 6, 'Reservas', 'mdi-phone', '/reservas', 1);
