-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.3
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---
-- object: usr_admin | type: ROLE --
-- DROP ROLE IF EXISTS usr_admin;
CREATE ROLE usr_admin WITH 
	CREATEDB
	CREATEROLE
	LOGIN
	ENCRYPTED PASSWORD 'Us44dM1n00785';
-- ddl-end --


-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- object: bd_dental | type: DATABASE --
-- DROP DATABASE IF EXISTS bd_dental;
CREATE DATABASE bd_dental WITH OWNER usr_admin;
-- ddl-end --


-- object: autenticacion | type: SCHEMA --
-- DROP SCHEMA IF EXISTS autenticacion CASCADE;
CREATE SCHEMA autenticacion;
-- ddl-end --
ALTER SCHEMA autenticacion OWNER TO usr_admin;
-- ddl-end --

-- object: parametricas | type: SCHEMA --
-- DROP SCHEMA IF EXISTS parametricas CASCADE;
CREATE SCHEMA parametricas;
-- ddl-end --
ALTER SCHEMA parametricas OWNER TO usr_admin;
-- ddl-end --

-- object: registro | type: SCHEMA --
-- DROP SCHEMA IF EXISTS registro CASCADE;
CREATE SCHEMA registro;
-- ddl-end --
ALTER SCHEMA registro OWNER TO usr_admin;
-- ddl-end --

SET search_path TO pg_catalog,public,autenticacion,parametricas,registro;
-- ddl-end --

-- object: parametricas.estado | type: TABLE --
-- DROP TABLE IF EXISTS parametricas.estado CASCADE;
CREATE TABLE parametricas.estado (
	est_codigo integer NOT NULL,
	est_nombre varchar NOT NULL,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	CONSTRAINT estado_pk PRIMARY KEY (est_codigo)

);
-- ddl-end --
COMMENT ON COLUMN parametricas.estado.est_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN parametricas.estado.est_nombre IS 'nombre del estado';
-- ddl-end --
ALTER TABLE parametricas.estado OWNER TO usr_admin;
-- ddl-end --

-- object: autenticacion.rol | type: TABLE --
-- DROP TABLE IF EXISTS autenticacion.rol CASCADE;
CREATE TABLE autenticacion.rol (
	rol_codigo integer NOT NULL,
	rol_nombre varchar NOT NULL,
	rol_descripcion varchar NOT NULL,
	rol_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT rol_pk PRIMARY KEY (rol_codigo)

);
-- ddl-end --
COMMENT ON COLUMN autenticacion.rol.rol_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN autenticacion.rol.rol_descripcion IS 'Descripcion del rol';
-- ddl-end --
ALTER TABLE autenticacion.rol OWNER TO usr_admin;
-- ddl-end --

-- object: autenticacion.menu | type: TABLE --
-- DROP TABLE IF EXISTS autenticacion.menu CASCADE;
CREATE TABLE autenticacion.menu (
	men_codigo integer NOT NULL,
	men_codigo_padre integer,
	men_orden integer NOT NULL,
	men_nombre varchar NOT NULL,
	men_icono varchar NOT NULL,
	men_ruta varchar NOT NULL,
	men_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT menu_pk PRIMARY KEY (men_codigo)

);
-- ddl-end --
COMMENT ON COLUMN autenticacion.menu.men_codigo IS 'idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN autenticacion.menu.men_codigo_padre IS 'Identificador del menu padre si lo tuviera';
-- ddl-end --
COMMENT ON COLUMN autenticacion.menu.men_orden IS 'orden de los menus';
-- ddl-end --
COMMENT ON COLUMN autenticacion.menu.men_nombre IS 'Nombre del menu';
-- ddl-end --
COMMENT ON COLUMN autenticacion.menu.men_icono IS 'Icono del menu';
-- ddl-end --
COMMENT ON COLUMN autenticacion.menu.men_ruta IS 'Ruta a la que el menu se redirigira.';
-- ddl-end --
ALTER TABLE autenticacion.menu OWNER TO usr_admin;
-- ddl-end --

-- object: autenticacion.rol_menu | type: TABLE --
-- DROP TABLE IF EXISTS autenticacion.rol_menu CASCADE;
CREATE TABLE autenticacion.rol_menu (
	rol_codigo integer NOT NULL,
	men_codigo integer NOT NULL,
	rolmen_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT rol_menu_pk PRIMARY KEY (rol_codigo,men_codigo)

);
-- ddl-end --
COMMENT ON COLUMN autenticacion.rol_menu.rol_codigo IS 'Referencia al rol';
-- ddl-end --
COMMENT ON COLUMN autenticacion.rol_menu.men_codigo IS 'referencia al menu';
-- ddl-end --
ALTER TABLE autenticacion.rol_menu OWNER TO usr_admin;
-- ddl-end --

-- object: autenticacion.usuario_rol | type: TABLE --
-- DROP TABLE IF EXISTS autenticacion.usuario_rol CASCADE;
CREATE TABLE autenticacion.usuario_rol (
	usu_codigo integer NOT NULL,
	rol_codigo integer NOT NULL,
	usurol_fechaexpiracion date NOT NULL,
	usurol_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT usuario_rol_pk PRIMARY KEY (usu_codigo,rol_codigo)

);
-- ddl-end --
COMMENT ON COLUMN autenticacion.usuario_rol.usu_codigo IS 'referencia al usuario';
-- ddl-end --
COMMENT ON COLUMN autenticacion.usuario_rol.rol_codigo IS 'referencia al rol';
-- ddl-end --
COMMENT ON COLUMN autenticacion.usuario_rol.usurol_fechaexpiracion IS 'fecha en la que el rol dejara de estar activa para el usuario';
-- ddl-end --
ALTER TABLE autenticacion.usuario_rol OWNER TO usr_admin;
-- ddl-end --

-- object: autenticacion.usuario | type: TABLE --
-- DROP TABLE IF EXISTS autenticacion.usuario CASCADE;
CREATE TABLE autenticacion.usuario (
	usu_codigo integer NOT NULL,
	per_codigo integer NOT NULL,
	usu_usuario varchar NOT NULL,
	usu_contrasenia varchar NOT NULL,
	usu_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT usuario_pk PRIMARY KEY (usu_codigo)

);
-- ddl-end --
COMMENT ON COLUMN autenticacion.usuario.usu_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN autenticacion.usuario.per_codigo IS 'identificador de la persona';
-- ddl-end --
COMMENT ON COLUMN autenticacion.usuario.usu_usuario IS 'nombre de usuario para el inicio de sesion.';
-- ddl-end --
COMMENT ON COLUMN autenticacion.usuario.usu_contrasenia IS 'contrasenia para el usuario';
-- ddl-end --
ALTER TABLE autenticacion.usuario OWNER TO usr_admin;
-- ddl-end --

-- object: registro.persona | type: TABLE --
-- DROP TABLE IF EXISTS registro.persona CASCADE;
CREATE TABLE registro.persona (
	per_codigo integer NOT NULL,
	tipdoc_codigo integer NOT NULL,
	gen_codigo integer NOT NULL DEFAULT 0,
	per_docidentidad varchar(20) NOT NULL,
	per_nombres varchar NOT NULL,
	per_primer_apellido varchar NOT NULL,
	per_segundo_apellido varchar,
	per_fecha_nacimiento date NOT NULL,
	per_celular varchar,
	per_correo varchar,
	per_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT persona_pk PRIMARY KEY (per_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_codigo IS 'idenitificador de personas';
-- ddl-end --
COMMENT ON COLUMN registro.persona.tipdoc_codigo IS 'Referencia al tipo de documento';
-- ddl-end --
COMMENT ON COLUMN registro.persona.gen_codigo IS 'Referencia al identificador de genero.';
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_docidentidad IS 'Numero del documento de identidad';
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_nombres IS 'Nombres de la persona';
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_primer_apellido IS 'Primer apellido o apellido paterno';
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_segundo_apellido IS 'Segundo apellido, apellido materno, apellido de casada o ambos (materno y casada)';
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_fecha_nacimiento IS 'Fecha de nacimiento';
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_celular IS 'Numero de celular personal de la persona';
-- ddl-end --
COMMENT ON COLUMN registro.persona.per_correo IS 'Direccion de correo electronico para la persona.';
-- ddl-end --
ALTER TABLE registro.persona OWNER TO usr_admin;
-- ddl-end --

-- object: registro.paciente | type: TABLE --
-- DROP TABLE IF EXISTS registro.paciente CASCADE;
CREATE TABLE registro.paciente (
	pac_codigo integer NOT NULL,
	per_codigo integer NOT NULL,
	estciv_codigo integer NOT NULL DEFAULT 0,
	pac_direccion varchar,
	pac_telefono varchar,
	pac_ocupacion varchar,
	pac_lugar_nacimiento varchar,
	pac_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT paciente_pk PRIMARY KEY (pac_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.paciente.pac_codigo IS 'identificador del registro.';
-- ddl-end --
COMMENT ON COLUMN registro.paciente.per_codigo IS 'Referencia al codigo de persona que se le asignara al paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.paciente.estciv_codigo IS 'Referencia estado civil';
-- ddl-end --
COMMENT ON COLUMN registro.paciente.pac_direccion IS 'Direccion del paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.paciente.pac_telefono IS 'Numero de telefono de referencia del paciente';
-- ddl-end --
COMMENT ON COLUMN registro.paciente.pac_ocupacion IS 'ocupacion del paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.paciente.pac_lugar_nacimiento IS 'Lugar de nacimiento del paciente.';
-- ddl-end --
ALTER TABLE registro.paciente OWNER TO usr_admin;
-- ddl-end --

-- object: registro.antecedente | type: TABLE --
-- DROP TABLE IF EXISTS registro.antecedente CASCADE;
CREATE TABLE registro.antecedente (
	ant_codigo integer NOT NULL,
	pac_codigo integer NOT NULL,
	ant_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT antecedente_pk PRIMARY KEY (ant_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.antecedente.ant_codigo IS 'Registro de antecedentes del paciente';
-- ddl-end --
COMMENT ON COLUMN registro.antecedente.pac_codigo IS 'Referecia del paciente.';
-- ddl-end --
ALTER TABLE registro.antecedente OWNER TO usr_admin;
-- ddl-end --

-- object: registro.antecedentes_patologicos | type: TABLE --
-- DROP TABLE IF EXISTS registro.antecedentes_patologicos CASCADE;
CREATE TABLE registro.antecedentes_patologicos (
	antpat_codigo integer NOT NULL,
	ant_codigo integer,
	antpat_familiares varchar NOT NULL,
	antpat_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT antecedentes_patologicos_pk PRIMARY KEY (antpat_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_patologicos.antpat_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_patologicos.ant_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_patologicos.antpat_familiares IS 'Referencia de antecedentes patologicos familiares.';
-- ddl-end --
ALTER TABLE registro.antecedentes_patologicos OWNER TO usr_admin;
-- ddl-end --

-- object: registro.antecedentes_personales | type: TABLE --
-- DROP TABLE IF EXISTS registro.antecedentes_personales CASCADE;
CREATE TABLE registro.antecedentes_personales (
	antper_codigo integer NOT NULL,
	ant_codigo integer,
	antper_hopitalizacion varchar,
	antper_atencion varchar,
	antper_alergias varchar,
	antper_infecciosos varchar,
	antper_alteraciones varchar,
	antper_tratamiento varchar,
	antper_medicacion varchar,
	antper_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT antecedentes_personales_pk PRIMARY KEY (antper_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.ant_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_hopitalizacion IS 'Descripcion de hospitalizaciones del paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_atencion IS 'Descripciones de atenciones medicas del paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_alergias IS 'alergias del paciente';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_infecciosos IS 'Infecciones del paciente';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_alteraciones IS 'decripcion de alteraciones hemorragicas del paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_tratamiento IS 'Descripcion de tratamientos actuales del paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_personales.antper_medicacion IS 'Descripcion de medicacion actual el paciente.';
-- ddl-end --
ALTER TABLE registro.antecedentes_personales OWNER TO usr_admin;
-- ddl-end --

-- object: registro.antecedentes_afecciones | type: TABLE --
-- DROP TABLE IF EXISTS registro.antecedentes_afecciones CASCADE;
CREATE TABLE registro.antecedentes_afecciones (
	antafe_codigo integer NOT NULL,
	ant_codigo integer,
	tipafe_codigo integer NOT NULL,
	antafe_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT antecedentes_afecciones_pk PRIMARY KEY (antafe_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_afecciones.antafe_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_afecciones.ant_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_afecciones.tipafe_codigo IS 'Referencia al tipo de afeccion.';
-- ddl-end --
ALTER TABLE registro.antecedentes_afecciones OWNER TO usr_admin;
-- ddl-end --

-- object: parametricas.tipo_afeccion | type: TABLE --
-- DROP TABLE IF EXISTS parametricas.tipo_afeccion CASCADE;
CREATE TABLE parametricas.tipo_afeccion (
	tipafe_codigo integer NOT NULL,
	tipafe_nombre varchar NOT NULL,
	tipafe_descripcion varchar,
	tipafe_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT tipo_afeccion_pk PRIMARY KEY (tipafe_codigo)

);
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_afeccion.tipafe_codigo IS 'Ifentificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_afeccion.tipafe_nombre IS 'Nombre del tipo de afeccion.';
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_afeccion.tipafe_descripcion IS 'descripcion adicional de la afeccion.';
-- ddl-end --
ALTER TABLE parametricas.tipo_afeccion OWNER TO usr_admin;
-- ddl-end --

-- object: registro.antecedentes_dentales | type: TABLE --
-- DROP TABLE IF EXISTS registro.antecedentes_dentales CASCADE;
CREATE TABLE registro.antecedentes_dentales (
	ant_codigo integer NOT NULL,
	antden_higiene varchar,
	antden_respiracion varchar,
	antden_onicofagia varchar,
	antden_queilifagia varchar,
	antden_bruxismo varchar,
	antden_cepillado varchar,
	antden_diagnostico varchar,
	antden_ultimo_tratamiento varchar,
	antden_observaciones varchar,
	antden_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT antedecentes_dentales_pk PRIMARY KEY (ant_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.ant_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_higiene IS 'Descripcion de antecedentes por higiene bucal';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_respiracion IS 'antedecentes por respiracion';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_onicofagia IS 'antecedentes de onicofagia';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_queilifagia IS 'antecedentes por queilofagia';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_bruxismo IS 'antecedentes por bruxismo';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_cepillado IS 'Veces por dia del cepillado.';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_diagnostico IS 'Diagnostico periodontal';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_ultimo_tratamiento IS 'Descripcion del ultimo tratamiento dental.';
-- ddl-end --
COMMENT ON COLUMN registro.antecedentes_dentales.antden_observaciones IS 'observaciones.';
-- ddl-end --
ALTER TABLE registro.antecedentes_dentales OWNER TO usr_admin;
-- ddl-end --

-- object: registro.tratamiento | type: TABLE --
-- DROP TABLE IF EXISTS registro.tratamiento CASCADE;
CREATE TABLE registro.tratamiento (
	tra_codigo integer NOT NULL,
	tiptra_codigo integer NOT NULL,
	pac_codigo integer NOT NULL,
	perodo_codigo integer NOT NULL,
	tra_total numeric(10,2) NOT NULL DEFAULT 0.0,
	tra_observaciones varchar,
	tra_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT tratamiento_pk PRIMARY KEY (tra_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento.tra_codigo IS 'Identificador del tratamiento';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento.tiptra_codigo IS 'Referencia al tipo de tratamiento.';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento.pac_codigo IS 'referencia al paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento.perodo_codigo IS 'Referencia al personal odontologico que realizara el tratamiento.';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento.tra_total IS 'Monto total del tratamiento';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento.tra_observaciones IS 'Observaciones al registrar el inicio del tratamiento';
-- ddl-end --
ALTER TABLE registro.tratamiento OWNER TO usr_admin;
-- ddl-end --

-- object: parametricas.tipo_tratamiento | type: TABLE --
-- DROP TABLE IF EXISTS parametricas.tipo_tratamiento CASCADE;
CREATE TABLE parametricas.tipo_tratamiento (
	tiptra_codigo integer NOT NULL,
	tiptra_nombre varchar NOT NULL,
	tiptra_descripcion varchar,
	tiptra_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT tipo_tratamiento_pk PRIMARY KEY (tiptra_codigo)

);
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_tratamiento.tiptra_codigo IS 'idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_tratamiento.tiptra_nombre IS 'Nombre del tratamiento';
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_tratamiento.tiptra_descripcion IS 'Descripcion del tratamiento.';
-- ddl-end --
ALTER TABLE parametricas.tipo_tratamiento OWNER TO usr_admin;
-- ddl-end --

-- object: registro.tratamiento_plan | type: TABLE --
-- DROP TABLE IF EXISTS registro.tratamiento_plan CASCADE;
CREATE TABLE registro.tratamiento_plan (
	trapla_codigo integer NOT NULL,
	tra_codigo integer NOT NULL,
	trapla_fecha date NOT NULL DEFAULT current_date,
	trapla_pieza varchar NOT NULL,
	trapla_diagnostico varchar,
	trapla_tratamiento varchar,
	trapla_acuenta numeric(10,2) NOT NULL DEFAULT 0.0,
	trapla_saldo numeric(10,2) NOT NULL,
	trapla_observaciones varchar,
	trapla_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT tratamiento_plan_pk PRIMARY KEY (trapla_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.tra_codigo IS 'Referencia la tratamiento.';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_fecha IS 'Fecha de registro del plan de tratamiento';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_pieza IS 'Pieza dental tratada';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_diagnostico IS 'Diagnostico al momento de registrar el plan de tratamiento.';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_tratamiento IS 'Descripcion del tratamiento y evolucion';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_acuenta IS 'monto economico a cuenta del tramiento';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_saldo IS 'Saldo al momento de registrar el tratamiento';
-- ddl-end --
COMMENT ON COLUMN registro.tratamiento_plan.trapla_observaciones IS 'Observaciones del tratamiento.';
-- ddl-end --
ALTER TABLE registro.tratamiento_plan OWNER TO usr_admin;
-- ddl-end --

-- object: registro.personal_odontologico | type: TABLE --
-- DROP TABLE IF EXISTS registro.personal_odontologico CASCADE;
CREATE TABLE registro.personal_odontologico (
	perodo_codigo integer NOT NULL,
	per_codigo integer NOT NULL,
	esp_codigo integer NOT NULL,
	perodo_turno varchar NOT NULL,
	perodo_hora_inicio time NOT NULL,
	perodo_hora_final time NOT NULL,
	perodo_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT personal_odontologico_pk PRIMARY KEY (perodo_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.personal_odontologico.perodo_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.personal_odontologico.per_codigo IS 'Referencia a la persona.';
-- ddl-end --
COMMENT ON COLUMN registro.personal_odontologico.esp_codigo IS 'Referencia a la especialidad del odontologo';
-- ddl-end --
COMMENT ON COLUMN registro.personal_odontologico.perodo_turno IS 'turno de atencion del personal odontologico';
-- ddl-end --
COMMENT ON COLUMN registro.personal_odontologico.perodo_hora_inicio IS 'Hora de inicio de la atencion en la jornada del odontologo.';
-- ddl-end --
COMMENT ON COLUMN registro.personal_odontologico.perodo_hora_final IS 'Hora final de atencion en la jornada del odontologo';
-- ddl-end --
ALTER TABLE registro.personal_odontologico OWNER TO usr_admin;
-- ddl-end --

-- object: parametricas.tipo_documento | type: TABLE --
-- DROP TABLE IF EXISTS parametricas.tipo_documento CASCADE;
CREATE TABLE parametricas.tipo_documento (
	tipdoc_codigo integer NOT NULL,
	tipdoc_nombre varchar NOT NULL,
	tipdoc_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT tipo_documento_pk PRIMARY KEY (tipdoc_codigo)

);
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_documento.tipdoc_codigo IS 'identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN parametricas.tipo_documento.tipdoc_nombre IS 'Nombre del tipo de documento.';
-- ddl-end --
ALTER TABLE parametricas.tipo_documento OWNER TO usr_admin;
-- ddl-end --

-- object: parametricas.estado_civil | type: TABLE --
-- DROP TABLE IF EXISTS parametricas.estado_civil CASCADE;
CREATE TABLE parametricas.estado_civil (
	estciv_codigo integer NOT NULL,
	estciv_nombre varchar NOT NULL,
	estciv_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT estado_civil_pk PRIMARY KEY (estciv_codigo)

);
-- ddl-end --
COMMENT ON COLUMN parametricas.estado_civil.estciv_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN parametricas.estado_civil.estciv_nombre IS 'nombre del estado civil';
-- ddl-end --
ALTER TABLE parametricas.estado_civil OWNER TO usr_admin;
-- ddl-end --

-- object: parametricas.genero | type: TABLE --
-- DROP TABLE IF EXISTS parametricas.genero CASCADE;
CREATE TABLE parametricas.genero (
	gen_codigo integer NOT NULL,
	gen_nombre varchar NOT NULL,
	gen_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT genero_pk PRIMARY KEY (gen_codigo)

);
-- ddl-end --
COMMENT ON COLUMN parametricas.genero.gen_codigo IS 'Identificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN parametricas.genero.gen_nombre IS 'Nombre del genero';
-- ddl-end --
COMMENT ON COLUMN parametricas.genero.gen_estado IS 'estado del genero';
-- ddl-end --
ALTER TABLE parametricas.genero OWNER TO usr_admin;
-- ddl-end --

-- object: registro.reservas | type: TABLE --
-- DROP TABLE IF EXISTS registro.reservas CASCADE;
CREATE TABLE registro.reservas (
	res_codigo integer NOT NULL,
	perodo_codigo integer NOT NULL,
	pac_codigo integer NOT NULL,
	res_fecha date NOT NULL,
	res_hora time NOT NULL,
	res_confirmacion boolean NOT NULL DEFAULT false,
	res_estado integer NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT reservas_pk PRIMARY KEY (res_codigo)

);
-- ddl-end --
COMMENT ON COLUMN registro.reservas.res_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN registro.reservas.perodo_codigo IS 'Referencia del personal odontologico';
-- ddl-end --
COMMENT ON COLUMN registro.reservas.pac_codigo IS 'Referencia la paciente.';
-- ddl-end --
COMMENT ON COLUMN registro.reservas.res_fecha IS 'Fecha de la reserva';
-- ddl-end --
COMMENT ON COLUMN registro.reservas.res_hora IS 'Hora de la reserva';
-- ddl-end --
COMMENT ON COLUMN registro.reservas.res_confirmacion IS 'Verifica si el paciente confirmo su cita.';
-- ddl-end --
COMMENT ON COLUMN registro.reservas.res_estado IS 'Estado de la reserva';
-- ddl-end --
ALTER TABLE registro.reservas OWNER TO usr_admin;
-- ddl-end --

-- object: parametricas.especialidad | type: TABLE --
-- DROP TABLE IF EXISTS parametricas.especialidad CASCADE;
CREATE TABLE parametricas.especialidad (
	esp_codigo integer NOT NULL,
	esp_nombre varchar NOT NULL,
	esp_estado smallint NOT NULL DEFAULT 1,
	usuario_registro integer NOT NULL,
	usuario_modificacion integer NOT NULL DEFAULT 0,
	usuario_baja integer NOT NULL DEFAULT 0,
	fecha_registro timestamp NOT NULL DEFAULT current_timestamp::timestamp WITHOUT time ZONE,
	fecha_modificacion timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	fecha_baja timestamp NOT NULL DEFAULT '1999-01-01'::timestamp WITHOUT time ZONE,
	CONSTRAINT especialidad_pk PRIMARY KEY (esp_codigo)

);
-- ddl-end --
COMMENT ON COLUMN parametricas.especialidad.esp_codigo IS 'Idenitificador de la tabla';
-- ddl-end --
COMMENT ON COLUMN parametricas.especialidad.esp_nombre IS 'Nombre de  la especialidad';
-- ddl-end --
ALTER TABLE parametricas.especialidad OWNER TO usr_admin;
-- ddl-end --

-- object: estado_rol_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.rol DROP CONSTRAINT IF EXISTS estado_rol_fk CASCADE;
ALTER TABLE autenticacion.rol ADD CONSTRAINT estado_rol_fk FOREIGN KEY (rol_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_menu_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.menu DROP CONSTRAINT IF EXISTS estado_menu_fk CASCADE;
ALTER TABLE autenticacion.menu ADD CONSTRAINT estado_menu_fk FOREIGN KEY (men_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: menu_menu_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.menu DROP CONSTRAINT IF EXISTS menu_menu_fk CASCADE;
ALTER TABLE autenticacion.menu ADD CONSTRAINT menu_menu_fk FOREIGN KEY (men_codigo)
REFERENCES autenticacion.menu (men_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_rol_menu_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.rol_menu DROP CONSTRAINT IF EXISTS estado_rol_menu_fk CASCADE;
ALTER TABLE autenticacion.rol_menu ADD CONSTRAINT estado_rol_menu_fk FOREIGN KEY (rolmen_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rol_rol_menu_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.rol_menu DROP CONSTRAINT IF EXISTS rol_rol_menu_fk CASCADE;
ALTER TABLE autenticacion.rol_menu ADD CONSTRAINT rol_rol_menu_fk FOREIGN KEY (rol_codigo)
REFERENCES autenticacion.rol (rol_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: menu_rol_menu_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.rol_menu DROP CONSTRAINT IF EXISTS menu_rol_menu_fk CASCADE;
ALTER TABLE autenticacion.rol_menu ADD CONSTRAINT menu_rol_menu_fk FOREIGN KEY (men_codigo)
REFERENCES autenticacion.menu (men_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: usuario_usuario_rol_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.usuario_rol DROP CONSTRAINT IF EXISTS usuario_usuario_rol_fk CASCADE;
ALTER TABLE autenticacion.usuario_rol ADD CONSTRAINT usuario_usuario_rol_fk FOREIGN KEY (usu_codigo)
REFERENCES autenticacion.usuario (usu_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rol_usuario_rol_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.usuario_rol DROP CONSTRAINT IF EXISTS rol_usuario_rol_fk CASCADE;
ALTER TABLE autenticacion.usuario_rol ADD CONSTRAINT rol_usuario_rol_fk FOREIGN KEY (rol_codigo)
REFERENCES autenticacion.rol (rol_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_usuerio_rol_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.usuario_rol DROP CONSTRAINT IF EXISTS estado_usuerio_rol_fk CASCADE;
ALTER TABLE autenticacion.usuario_rol ADD CONSTRAINT estado_usuerio_rol_fk FOREIGN KEY (usurol_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: persona_usuario_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.usuario DROP CONSTRAINT IF EXISTS persona_usuario_fk CASCADE;
ALTER TABLE autenticacion.usuario ADD CONSTRAINT persona_usuario_fk FOREIGN KEY (per_codigo)
REFERENCES registro.persona (per_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_usuario_fk | type: CONSTRAINT --
-- ALTER TABLE autenticacion.usuario DROP CONSTRAINT IF EXISTS estado_usuario_fk CASCADE;
ALTER TABLE autenticacion.usuario ADD CONSTRAINT estado_usuario_fk FOREIGN KEY (usu_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_persona | type: CONSTRAINT --
-- ALTER TABLE registro.persona DROP CONSTRAINT IF EXISTS estado_persona CASCADE;
ALTER TABLE registro.persona ADD CONSTRAINT estado_persona FOREIGN KEY (per_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: tipo_documento_persona_fk | type: CONSTRAINT --
-- ALTER TABLE registro.persona DROP CONSTRAINT IF EXISTS tipo_documento_persona_fk CASCADE;
ALTER TABLE registro.persona ADD CONSTRAINT tipo_documento_persona_fk FOREIGN KEY (tipdoc_codigo)
REFERENCES parametricas.tipo_documento (tipdoc_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: genero_persona_fk | type: CONSTRAINT --
-- ALTER TABLE registro.persona DROP CONSTRAINT IF EXISTS genero_persona_fk CASCADE;
ALTER TABLE registro.persona ADD CONSTRAINT genero_persona_fk FOREIGN KEY (gen_codigo)
REFERENCES parametricas.genero (gen_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: persona_paciente_fk | type: CONSTRAINT --
-- ALTER TABLE registro.paciente DROP CONSTRAINT IF EXISTS persona_paciente_fk CASCADE;
ALTER TABLE registro.paciente ADD CONSTRAINT persona_paciente_fk FOREIGN KEY (per_codigo)
REFERENCES registro.persona (per_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_paciente | type: CONSTRAINT --
-- ALTER TABLE registro.paciente DROP CONSTRAINT IF EXISTS estado_paciente CASCADE;
ALTER TABLE registro.paciente ADD CONSTRAINT estado_paciente FOREIGN KEY (pac_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_civil_paciente_fk | type: CONSTRAINT --
-- ALTER TABLE registro.paciente DROP CONSTRAINT IF EXISTS estado_civil_paciente_fk CASCADE;
ALTER TABLE registro.paciente ADD CONSTRAINT estado_civil_paciente_fk FOREIGN KEY (estciv_codigo)
REFERENCES parametricas.estado_civil (estciv_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: paciente_antecedentes_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedente DROP CONSTRAINT IF EXISTS paciente_antecedentes_fk CASCADE;
ALTER TABLE registro.antecedente ADD CONSTRAINT paciente_antecedentes_fk FOREIGN KEY (pac_codigo)
REFERENCES registro.paciente (pac_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_antecedentes_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedente DROP CONSTRAINT IF EXISTS estado_antecedentes_fk CASCADE;
ALTER TABLE registro.antecedente ADD CONSTRAINT estado_antecedentes_fk FOREIGN KEY (ant_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: antecedentes_antecedentes_patologicos_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_patologicos DROP CONSTRAINT IF EXISTS antecedentes_antecedentes_patologicos_fk CASCADE;
ALTER TABLE registro.antecedentes_patologicos ADD CONSTRAINT antecedentes_antecedentes_patologicos_fk FOREIGN KEY (ant_codigo)
REFERENCES registro.antecedente (ant_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_antecedentes_patologicos_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_patologicos DROP CONSTRAINT IF EXISTS estado_antecedentes_patologicos_fk CASCADE;
ALTER TABLE registro.antecedentes_patologicos ADD CONSTRAINT estado_antecedentes_patologicos_fk FOREIGN KEY (antpat_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_antecedentes_personales_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_personales DROP CONSTRAINT IF EXISTS estado_antecedentes_personales_fk CASCADE;
ALTER TABLE registro.antecedentes_personales ADD CONSTRAINT estado_antecedentes_personales_fk FOREIGN KEY (antper_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: antecedentes_antecedentes_personales_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_personales DROP CONSTRAINT IF EXISTS antecedentes_antecedentes_personales_fk CASCADE;
ALTER TABLE registro.antecedentes_personales ADD CONSTRAINT antecedentes_antecedentes_personales_fk FOREIGN KEY (ant_codigo)
REFERENCES registro.antecedente (ant_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_antecedentes_afecciones_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_afecciones DROP CONSTRAINT IF EXISTS estado_antecedentes_afecciones_fk CASCADE;
ALTER TABLE registro.antecedentes_afecciones ADD CONSTRAINT estado_antecedentes_afecciones_fk FOREIGN KEY (antafe_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: antecedentes_antecedentes_afecciones_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_afecciones DROP CONSTRAINT IF EXISTS antecedentes_antecedentes_afecciones_fk CASCADE;
ALTER TABLE registro.antecedentes_afecciones ADD CONSTRAINT antecedentes_antecedentes_afecciones_fk FOREIGN KEY (ant_codigo)
REFERENCES registro.antecedente (ant_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: tipo_afeccion_antecedentes_afecciones_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_afecciones DROP CONSTRAINT IF EXISTS tipo_afeccion_antecedentes_afecciones_fk CASCADE;
ALTER TABLE registro.antecedentes_afecciones ADD CONSTRAINT tipo_afeccion_antecedentes_afecciones_fk FOREIGN KEY (tipafe_codigo)
REFERENCES parametricas.tipo_afeccion (tipafe_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_tipo_afeccion_fk | type: CONSTRAINT --
-- ALTER TABLE parametricas.tipo_afeccion DROP CONSTRAINT IF EXISTS estado_tipo_afeccion_fk CASCADE;
ALTER TABLE parametricas.tipo_afeccion ADD CONSTRAINT estado_tipo_afeccion_fk FOREIGN KEY (tipafe_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: antecedente_antecedentes_dentales_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_dentales DROP CONSTRAINT IF EXISTS antecedente_antecedentes_dentales_fk CASCADE;
ALTER TABLE registro.antecedentes_dentales ADD CONSTRAINT antecedente_antecedentes_dentales_fk FOREIGN KEY (ant_codigo)
REFERENCES registro.antecedente (ant_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_antecedentes_dentales_fk | type: CONSTRAINT --
-- ALTER TABLE registro.antecedentes_dentales DROP CONSTRAINT IF EXISTS estado_antecedentes_dentales_fk CASCADE;
ALTER TABLE registro.antecedentes_dentales ADD CONSTRAINT estado_antecedentes_dentales_fk FOREIGN KEY (antden_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: paciente_tratamiento_fk | type: CONSTRAINT --
-- ALTER TABLE registro.tratamiento DROP CONSTRAINT IF EXISTS paciente_tratamiento_fk CASCADE;
ALTER TABLE registro.tratamiento ADD CONSTRAINT paciente_tratamiento_fk FOREIGN KEY (pac_codigo)
REFERENCES registro.paciente (pac_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: tipo_tratamiento_tratamiento_fk | type: CONSTRAINT --
-- ALTER TABLE registro.tratamiento DROP CONSTRAINT IF EXISTS tipo_tratamiento_tratamiento_fk CASCADE;
ALTER TABLE registro.tratamiento ADD CONSTRAINT tipo_tratamiento_tratamiento_fk FOREIGN KEY (tiptra_codigo)
REFERENCES parametricas.tipo_tratamiento (tiptra_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: personal_odontologico_tratamiento_fk | type: CONSTRAINT --
-- ALTER TABLE registro.tratamiento DROP CONSTRAINT IF EXISTS personal_odontologico_tratamiento_fk CASCADE;
ALTER TABLE registro.tratamiento ADD CONSTRAINT personal_odontologico_tratamiento_fk FOREIGN KEY (perodo_codigo)
REFERENCES registro.personal_odontologico (perodo_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: tratamiento_estado_fk | type: CONSTRAINT --
-- ALTER TABLE registro.tratamiento DROP CONSTRAINT IF EXISTS tratamiento_estado_fk CASCADE;
ALTER TABLE registro.tratamiento ADD CONSTRAINT tratamiento_estado_fk FOREIGN KEY (tra_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_tipo_tratamiento_fk | type: CONSTRAINT --
-- ALTER TABLE parametricas.tipo_tratamiento DROP CONSTRAINT IF EXISTS estado_tipo_tratamiento_fk CASCADE;
ALTER TABLE parametricas.tipo_tratamiento ADD CONSTRAINT estado_tipo_tratamiento_fk FOREIGN KEY (tiptra_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: tratamiento_tratamiento_plan_fk | type: CONSTRAINT --
-- ALTER TABLE registro.tratamiento_plan DROP CONSTRAINT IF EXISTS tratamiento_tratamiento_plan_fk CASCADE;
ALTER TABLE registro.tratamiento_plan ADD CONSTRAINT tratamiento_tratamiento_plan_fk FOREIGN KEY (tra_codigo)
REFERENCES registro.tratamiento (tra_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_tratamiento_plan_fk | type: CONSTRAINT --
-- ALTER TABLE registro.tratamiento_plan DROP CONSTRAINT IF EXISTS estado_tratamiento_plan_fk CASCADE;
ALTER TABLE registro.tratamiento_plan ADD CONSTRAINT estado_tratamiento_plan_fk FOREIGN KEY (trapla_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: persona_personal_odontologico_fk | type: CONSTRAINT --
-- ALTER TABLE registro.personal_odontologico DROP CONSTRAINT IF EXISTS persona_personal_odontologico_fk CASCADE;
ALTER TABLE registro.personal_odontologico ADD CONSTRAINT persona_personal_odontologico_fk FOREIGN KEY (per_codigo)
REFERENCES registro.persona (per_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_personal_odontologico_fk | type: CONSTRAINT --
-- ALTER TABLE registro.personal_odontologico DROP CONSTRAINT IF EXISTS estado_personal_odontologico_fk CASCADE;
ALTER TABLE registro.personal_odontologico ADD CONSTRAINT estado_personal_odontologico_fk FOREIGN KEY (perodo_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: especialidad_personal_odontologico_fk | type: CONSTRAINT --
-- ALTER TABLE registro.personal_odontologico DROP CONSTRAINT IF EXISTS especialidad_personal_odontologico_fk CASCADE;
ALTER TABLE registro.personal_odontologico ADD CONSTRAINT especialidad_personal_odontologico_fk FOREIGN KEY (esp_codigo)
REFERENCES parametricas.especialidad (esp_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_tipo_documento_fk | type: CONSTRAINT --
-- ALTER TABLE parametricas.tipo_documento DROP CONSTRAINT IF EXISTS estado_tipo_documento_fk CASCADE;
ALTER TABLE parametricas.tipo_documento ADD CONSTRAINT estado_tipo_documento_fk FOREIGN KEY (tipdoc_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_estado_civil_fk | type: CONSTRAINT --
-- ALTER TABLE parametricas.estado_civil DROP CONSTRAINT IF EXISTS estado_estado_civil_fk CASCADE;
ALTER TABLE parametricas.estado_civil ADD CONSTRAINT estado_estado_civil_fk FOREIGN KEY (estciv_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_genero_fk | type: CONSTRAINT --
-- ALTER TABLE parametricas.genero DROP CONSTRAINT IF EXISTS estado_genero_fk CASCADE;
ALTER TABLE parametricas.genero ADD CONSTRAINT estado_genero_fk FOREIGN KEY (gen_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: personal_odontologico_reserva_fk | type: CONSTRAINT --
-- ALTER TABLE registro.reservas DROP CONSTRAINT IF EXISTS personal_odontologico_reserva_fk CASCADE;
ALTER TABLE registro.reservas ADD CONSTRAINT personal_odontologico_reserva_fk FOREIGN KEY (perodo_codigo)
REFERENCES registro.personal_odontologico (perodo_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: paciente_reserva_fk | type: CONSTRAINT --
-- ALTER TABLE registro.reservas DROP CONSTRAINT IF EXISTS paciente_reserva_fk CASCADE;
ALTER TABLE registro.reservas ADD CONSTRAINT paciente_reserva_fk FOREIGN KEY (pac_codigo)
REFERENCES registro.paciente (pac_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_reserva_fk | type: CONSTRAINT --
-- ALTER TABLE registro.reservas DROP CONSTRAINT IF EXISTS estado_reserva_fk CASCADE;
ALTER TABLE registro.reservas ADD CONSTRAINT estado_reserva_fk FOREIGN KEY (res_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: estado_especialidad_fk | type: CONSTRAINT --
-- ALTER TABLE parametricas.especialidad DROP CONSTRAINT IF EXISTS estado_especialidad_fk CASCADE;
ALTER TABLE parametricas.especialidad ADD CONSTRAINT estado_especialidad_fk FOREIGN KEY (esp_estado)
REFERENCES parametricas.estado (est_codigo) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


